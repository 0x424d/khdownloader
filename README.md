# khdownloader
A simple script to download albums from downloads.khinsider.com without paying the subscription fee.

## TODO
* Check supplied album page does actually exist (nonexistent albums unfortunately don't return 404)
* Log file
* Switches
	* -q / --quiet
		* Quiet output
	* -s / --silent
		* No output
	* -v / --verbose
		* Verbose output
	* -d / --debug
		* Debug output
	* -f / --filetype
		* Try to download specified file type. Default to mp3 if file type is not found.
	* -l / --logfile
		* Specify path of log file
* Allow multiple Boolean switches to be specified with one argument (e.g. make `./khdownloader.py -abc` equivalent to `./khdownloader -a -b -c`)
* Stop the user from specifying mutually exclusive switches at the same time (e.g. `./khdownloader.py -vq`)