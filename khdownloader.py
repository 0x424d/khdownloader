# Exit codes:
#	0: Normal exit
#	1: Script was invoked with too few command-line arguments
#	2: Script was not supplied with any URLs
#	3: User declined to continue at a prompt
#	4: Could not find album at supplied URL
#	-1: Unknown error

import requests, sys, os, re
from bs4 import BeautifulSoup
from math import ceil
from urllib.parse import unquote

# util function, simple confirmation dialogue
def confirmOk(tabCount, autoconfirm):
	print("\n" + "\t" * tabCount + "Continue? (Y / N)")
	
	# extra visual feedback
	if autoconfirm:
		print("\t" * tabCount + "> y")
		return
		
	while True:
		response = input("\t" * tabCount + "> ")
		if response.lower() == "y":
			return
		elif response.lower() == "n":
			print("\t" * tabCount + "Exiting...")
			exit(3)
			
# util function, pretty print to terminal
def pprint(string):
	string = string.expandtabs()

	print(string, end="", flush=True)
	print(" " * (os.get_terminal_size()[0] - len(string)), end="", flush=True)
	print("\b" * (os.get_terminal_size()[0]), end="", flush=True)
		
# parse command-line arguments
def parse():
	args = {"urls": []}
	
	# if the user just enters ./khdownloader.py
	if len(sys.argv) < 2:
		print("Usage: ./khdownloader.py [options...] <url>")
		print("Type ./khdownloader -h for options.")
		print("Exiting...")
		exit(1)
	
	# help message
	if "-h" in sys.argv or "--help" in sys.argv:
		print("Usage: ./khdownloader.py [options...] <url>")
		print("-c / --chunksize: Specify chunk size for downloads")
		print("-y / --yes: Assume yes to every 'Continue?' prompt")
		print("Exiting...")
		exit(0)
	
	args["urls"] = re.findall(r"https?://downloads.khinsider.com/game-soundtracks/album/[^ ]+", " ".join(sys.argv))
	
	# if args is still an empty list, aka no compatible URLs were found
	if not args["urls"]:
		print("No compatible URL supplied. Please ensure your URL(s) begin with http:// or https:// and refer to a khinsider album page.")
		print("Exiting...")
		exit(2)
	
	# make short and long arguments equivalent
	sys.argv = list(map(lambda s: s.replace("--chunksize", "-c"), sys.argv))
	sys.argv = list(map(lambda s: s.replace("--yes", "-y"), sys.argv))
	sys.argv = list(map(lambda s: s.replace("--filetype", "-f"), sys.argv))
	
	args["autoconfirm"] = "-y" in sys.argv
	
	try:
		args["chunksize"] = int(sys.argv[sys.argv.index("-c")+1])
		assert args["chunksize"] > 0
	except IndexError as e: # if the user passes something like ./khdownloader.py <url> -c
		print("Chunk size switch was provided, but given no argument. Assuming 512.")
		confirmOk(0, args["autoconfirm"])
	except AssertionError as e: # if chunk size <= 0
		print("Chunk size must be greater than 0. Assuming 512.")
		confirmOk(0, args["autoconfirm"])
	except ValueError as e:
		# if switch was found, but passed value was not an integer (e.args takes the form of a tuple)
		if e.args[0].startswith("invalid literal for int() with base 10:"):
			print("Chunk size must be an integer. Assuming 512.")
			confirmOk(0, args["autoconfirm"])
		# if -c was not in list - assume defaults so no confirmation
		elif e.args[0].endswith("is not in list"):
			print("No chunk size provided. Assuming 512.")
		else:
			print(e)
			exit(-1)
	else: # if there were no errors
		return args
	
	# if there were errors and the program was not stopped (by confirmOk() function)
	args["chunksize"] = 512
	return args

# get all the tracks on the given album page
def getTrackLinks():
	args = parse()
	for n, i in enumerate(args["urls"], 1):
		print(f"\nDownloading album {n} / {len(args['urls'])}...")
		
		doc            = requests.get(i)
		albumSoup      = BeautifulSoup(doc.text, "lxml")
		trackPageLinks = albumSoup.find(id='songlist').find_all('a')

		# Set ensures there are no duplicate links
		linksSet = {"http://downloads.khinsider.com" + j['href'] for j in trackPageLinks}
		
		getDownloadLinks(sorted(linksSet), albumSoup, args)
		
	print("Finished downloading everything!")

# get download links for every individual track
def getDownloadLinks(linksSet, albumSoup, args):
	downloadLinks = []
	for n, url in enumerate(linksSet, 1):
		pprint(f"\tLooking for download links for track {n} / {len(linksSet)}... "
				+ unquote(unquote(url.split('/')[-1])))
		
		doc       = requests.get(url)
		trackSoup = BeautifulSoup(doc.text, "lxml")
		
		for i in trackSoup.find_all(class_="songDownloadLink"):
			downloadLinks.append(i.parent['href'])

	formats     = {i.split(".")[-1] for i in downloadLinks}
	data        = getAlbumData(albumSoup)
	totalTracks = int(data['Number of Files'])
	tracksFound = len(downloadLinks) // len(formats)
	percentage  = round((tracksFound / totalTracks) * 100, 2)
	
	print(f"\n\n\tFound {tracksFound} tracks. This is {percentage}% of the total album.")
	
	if len(formats) == 1:
		choice = formats.pop()
	else:
		print(f"\tFound {len(formats)} formats:")
		for i in formats: print(f"\t\t{i}")
		print("\tPlease enter the format you would like to download.")
		while True:
			choice = input("\t> ")
			if choice.lower() in formats:
				break
			
	downloadLinks = list(filter(lambda s: s.endswith(choice), downloadLinks))
	
	print(f"\n\tAlbum name: {data['Album name']}")
	print(f"\tFormat: {choice}")
	print(f"\tAlbum size: {data['Total Filesize']}")
	print(f"\tChunk size: {args['chunksize']}")
	confirmOk(1, args["autoconfirm"])
	
	dl(downloadLinks, data, choice, args)

# at the top of each album page, there is a small synopsis of the album which includes a few relevant data points for the
# downloader. this function scrapes the relevant information from that synopsis
def getAlbumData(soup):
	# Luckily this gives a unique value which is exactly what I'm looking for
	dataParent = soup.find(id="EchoTopic").find('p', align='left')
	dataList = []
	dataDict = {}
	
	# messy and thoroughly untested cleanup; may break if the format isn't uniform across every page
	dataList = dataParent.get_text().replace("\r\n", "\n").replace("\t", "").strip().split("\n")

	for line in dataList:
		j = line.split(": ")
		dataDict[j[0]] = j[1]
		
	return dataDict
	
# creates a local directory for the album and downloads each track using the requests library
def dl(downloadLinks, data, choice, args):
	if not os.path.exists(data["Album name"]):
		os.mkdir(data["Album name"])
	
	for n, i in enumerate(downloadLinks, 1):
		print(f"\tGetting page {n} / {len(downloadLinks)}...")
		r = requests.get(i)
		
		filesize    = len(r.content)
		filename    = data['Album name'] + os.sep + unquote(i.split('/')[-1])
		percentDone = (n / len(downloadLinks)) * 100
		
		try:
			if os.path.getsize(filename) == filesize:
				print(f"\t\tSkipping {filename}. File already exists.")
				continue
		except OSError:
			pass
		
		print(f"\tDownloading file {n} / {len(downloadLinks)} ({round(percentDone, 2)}%)...")		
		print(f"\t\tName: {filename}\n\t\tSize: {round(filesize / 1024 / 1024, 2)} MB\n")
		
		with open(filename, "wb") as f:
			m = 1
			for chunk in r.iter_content(args["chunksize"]):
				if chunk:
					pprint(f"\t\tWriting chunk {m} / {ceil(filesize / args['chunksize'])}...")
					f.write(chunk)
					m += 1
			print()
			#print(f"\n\t\tFinished downloading {filename}!")
	print("\n\tFinished downloading everything in this album!")

if __name__ == "__main__":
	getTrackLinks()

#mp3Links = ["https://vgmdownloads.com/soundtracks/pokemon-heartgold-and-soulsilver-music-super-complete/pkpmipijxq/1-01%20Opening%20Demo.mp3", "https://vgmdownloads.com/soundtracks/pokemon-heartgold-and-soulsilver-music-super-complete/ljgrcmrfen/2-07%20Battle%21%20Wild%20Pok%C3%A9mon%20%28Kanto%29.mp3"]
#dl(mp3Links)